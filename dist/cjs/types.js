"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DOCTYPESLOOKUP = void 0;
exports.DOCTYPESLOOKUP = {
    1: {
        title: 'ΑΤ ΑΣΤΥΝΟΜΙΚΗ ΤΑΥΤΟΤΗΤΑ',
        description: 'ΔΕΛΤΙΟ ΑΣΤΥΝΟΜΙΚΗΣ ΤΑΥΤΟΤΗΤΑΣ ΠΟΛΙΤΩΝ',
        authority: 'ΕΛΛΗΝΙΚΗ ΑΣΤΥΝΟΜΙΑ',
        service: 'searchIdentitiesById',
    },
    2: {
        title: 'ΕΣ ΕΛΛΗΝΙΚΟΣ ΣΤΡΑΤΟΣ',
        description: 'ΔΕΛΤΙΟ ΤΑΥΤΟΤΗΤΑΣ ΣΤΕΛΕΧΩΝ ΕΛΛΗΝΙΚΟΥ ΣΤΡΑΤΟΥ',
        authority: 'ΣΤΡΑΤΙΩΤΙΚΗ ΥΠΗΡΕΣΙΑ',
        service: 'searchIdentitiesByIdArmy',
    },
    3: {
        title: 'ΠΝ ΠΟΛΕΜΙΚΟ ΝΑΥΤΙΚΟ',
        description: 'ΔΕΛΤΙΟ ΤΑΥΤΟΤΗΤΑΣ ΣΤΕΛΕΧΩΝ ΠΟΛΕΜΙΚΟΥ ΝΑΥΤΙΚΟΥ',
        authority: 'ΣΤΡΑΤΙΩΤΙΚΗ ΥΠΗΡΕΣΙΑ',
        service: 'searchIdentitiesByIdArmy',
    },
    4: {
        title: 'ΠΑ ΠΟΛΕΜΙΚΗ ΑΕΡΟΠΟΡΙΑ',
        description: 'ΔΕΛΤΙΟ ΤΑΥΤΟΤΗΤΑΣ ΣΤΕΛΕΧΩΝ ΠΟΛΕΜΙΚΗΣ ΑΕΡΟΠΟΡΙΑΣ',
        authority: 'ΣΤΡΑΤΙΩΤΙΚΗ ΥΠΗΡΕΣΙΑ',
        service: 'searchIdentitiesByIdArmy',
    },
    5: {
        title: 'ΕΑ ΕΛΛΗΝΙΚΗ ΑΣΤΥΝΟΜΙΑ',
        description: 'ΔΕΛΤΙΟ ΤΑΥΤΟΤΗΤΑΣ ΕΝΣΤΟΛΩΝ/ΑΣΤΥΝΟΜΙΚΩΝ',
        authority: 'ΕΛΛΗΝΙΚΗ ΑΣΤΥΝΟΜΙΑ',
        service: 'searchIdentitiesByIdPol',
    },
    6: {
        title: 'ΛΣ ΛΙΜΕΝΙΚΟ ΣΩΜΑ',
        description: 'ΔΕΛΤΙΟ ΤΑΥΤΟΤΗΤΑΣ ΣΤΕΛΕΧΩΝ ΛΙΜΕΝΙΚΟΥ ΣΩΜΑΤΟΣ',
        authority: 'ΛΙΜΕΝΙΚΟ ΣΩΜΑ',
        service: 'searchIdentitiesByIdHcg',
    },
    7: {
        title: 'ΠΣ ΠΥΡΟΣΒΕΣΤΙΚΟ ΣΩΜΑ',
        description: 'ΔΕΛΤΙΟ ΤΑΥΤΟΤΗΤΑΣ ΣΤΕΛΕΧΩΝ ΠΥΡΟΣΒΕΣΤΙΚΟΥ ΣΩΜΑΤΟΣ',
        authority: 'ΠΥΡΟΣΒΕΣΤΙΚΗ ΥΠΗΡΕΣΙΑ',
        service: 'searchIdentitiesByIdFire',
    },
    8: {
        title: 'ΔΙ ΔΙΑΒΑΤΗΡΙΟ',
        description: 'ΕΛΛΗΝΙΚΟ ΔΙΑΒΑΤΗΡΙΟ',
        authority: 'ΕΛΛΗΝΙΚΗ ΑΣΤΥΝΟΜΙΑ',
        service: 'findPassportByParameters',
    },
    13: {
        title: 'ΒΕΒΑΙΩΣΗ ΕΥΡΩΠΑΙΩΝ ΠΟΛΙΤΩΝ',
        description: 'EE ΒΕΒΑΙΩΣΗ ΕΓΓΡΑΦΗΣ ΠΟΛΙΤΗ ΚΡΑΤΟΥΣ ΜΕΛΟΥΣ ΕΥΡΩΠΑΙΚΗΣ ΕΝΩΣΗΣ',
        authority: 'ΕΛΛΗΝΙΚΗ ΑΣΤΥΝΟΜΙΑ',
        service: 'getPersonalDetailsEE',
    },
    200: {
        title: 'ΕΣ ΓΕΕΘΑ',
        description: 'ΔΕΛΤΙΟ ΤΑΥΤΟΤΗΤΑΣ ΣΤΕΛΕΧΩΝ ΓΕΕΘΑ',
        authority: 'ΣΤΡΑΤΙΩΤΙΚΗ ΥΠΗΡΕΣΙΑ',
        service: 'searchIdentitiesByIdArmy',
    },
};
