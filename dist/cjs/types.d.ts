import { AuditRecord } from '@digigov-oss/gsis-audit-record-db';
type Enumerate<N extends number, Acc extends number[] = []> = Acc['length'] extends N ? Acc[number] : Enumerate<N, [...Acc, Acc['length']]>;
export type IntRange<F extends number, T extends number> = Exclude<Enumerate<T>, Enumerate<F>>;
export declare const DOCTYPESLOOKUP: {
    1: {
        title: string;
        description: string;
        authority: string;
        service: string;
    };
    2: {
        title: string;
        description: string;
        authority: string;
        service: string;
    };
    3: {
        title: string;
        description: string;
        authority: string;
        service: string;
    };
    4: {
        title: string;
        description: string;
        authority: string;
        service: string;
    };
    5: {
        title: string;
        description: string;
        authority: string;
        service: string;
    };
    6: {
        title: string;
        description: string;
        authority: string;
        service: string;
    };
    7: {
        title: string;
        description: string;
        authority: string;
        service: string;
    };
    8: {
        title: string;
        description: string;
        authority: string;
        service: string;
    };
    13: {
        title: string;
        description: string;
        authority: string;
        service: string;
    };
    200: {
        title: string;
        description: string;
        authority: string;
        service: string;
    };
};
export type ErrorRecord = {
    errorCode: string;
    errorDescr: string;
};
export type ValidateIDInputRecord = {
    id: string;
    type: IntRange<1, 9> | 13 | 200;
    reason: string;
};
export type ValidateIDInputRequest = {
    auditRecord: AuditRecord;
    validateIDInputRecord: ValidateIDInputRecord;
};
export type ValidateIDOutputRecord = {
    isFoundFlag: boolean;
    isValidFlag?: boolean;
    idOut?: string;
    issueDate?: string;
    issueInstitutionId?: string;
    issueInstitutionDesc?: string;
    expDate?: string;
    name?: string;
    surname?: string;
    fatherName?: string;
    motherName?: string;
    nameLatin?: string;
    surnameLatin?: string;
    fatherNameLatin?: string;
    gender?: string;
    birthDate?: string;
    birthPlace?: string;
    serviceCalled: string;
    idTypeDescr: string;
    message?: string;
};
export type ValidateIDResponse = {
    validateIDOutputRecord: ValidateIDOutputRecord;
    callSequenceId: string;
    callSequenceDate: string;
    errorRecord?: ErrorRecord;
};
export type KEDResponse = ValidateIDResponse;
export type ValidateIDOutput = {
    kedResponse: KEDResponse;
    auditRecord: AuditRecord;
};
export {};
