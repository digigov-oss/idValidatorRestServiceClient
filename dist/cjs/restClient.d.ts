import type { AuditRecord } from '@digigov-oss/gsis-audit-record-db';
import type { IntRange, ValidateIDResponse } from './types';
export declare class RestClient {
    user: string;
    pass: string;
    endpoint: string;
    constructor(user: string, pass: string, endpoint: string);
    validateID: (id: string, type: IntRange<1, 9> | 13 | 200, reason: string, auditRecord: AuditRecord) => Promise<ValidateIDResponse>;
}
export default RestClient;
