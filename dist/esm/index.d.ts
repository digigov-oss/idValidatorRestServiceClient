import type { IntRange, ValidateIDOutput } from './types.js';
import { AuditRecord, AuditEngine } from '@digigov-oss/gsis-audit-record-db';
/**
 * @type Overrides
 * @description Overrides for the REST client
 * @param {string} endpoint - Endpoint to be used for the REST client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 * @param {AuditEngine} auditEngine - Audit engine to be used for the audit record produced
 */
export type Overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};
declare class ValidateID {
    endpoint: string;
    prod: boolean;
    auditInit: object;
    auditStoragePath: string;
    auditEngine: AuditEngine;
    user: string;
    pass: string;
    constructor(user: string, pass: string, overrides?: Overrides);
    genAuditRecord: (overrides?: Overrides) => Promise<AuditRecord>;
    validateID: (id: string, type: IntRange<1, 9> | 200, reason: string, overrides?: Overrides) => Promise<ValidateIDOutput>;
}
export { DOCTYPESLOOKUP } from './types.js';
export type { ValidateIDInputRecord, ValidateIDResponse, ValidateIDOutputRecord, ValidateIDOutput, IntRange, KEDResponse, ErrorRecord, } from './types.js';
export default ValidateID;
