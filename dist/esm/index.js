import { generateAuditRecord, FileEngine, } from '@digigov-oss/gsis-audit-record-db';
import { RestClient } from './restClient.js';
import config from './config.json';
class ValidateID {
    endpoint;
    prod;
    auditInit;
    auditStoragePath;
    auditEngine;
    user;
    pass;
    constructor(user, pass, overrides) {
        this.prod = overrides?.prod ?? false;
        if (overrides?.endpoint) {
            this.endpoint = overrides?.endpoint;
        }
        else {
            this.endpoint = this.prod ? config.prod : config.test;
        }
        this.auditInit = overrides?.auditInit ?? {};
        this.auditStoragePath = overrides?.auditStoragePath ?? '/tmp';
        this.auditEngine =
            overrides?.auditEngine ?? new FileEngine(this.auditStoragePath);
        this.user = user;
        this.pass = pass;
    }
    genAuditRecord = async (overrides) => {
        const auditInit = Object.assign({}, this.auditInit, overrides?.auditInit);
        const auditRecord = await generateAuditRecord(auditInit, this.auditEngine);
        if (!auditRecord)
            throw new Error('Audit record is not initialized');
        return auditRecord;
    };
    validateID = async (id, type, reason, overrides) => {
        const auditRecord = await this.genAuditRecord(overrides);
        const restClient = new RestClient(this.user, this.pass, this.endpoint);
        const output = await restClient.validateID(id, type, reason, auditRecord);
        return { kedResponse: output, auditRecord: auditRecord };
    };
}
export { DOCTYPESLOOKUP } from './types.js';
export default ValidateID;
