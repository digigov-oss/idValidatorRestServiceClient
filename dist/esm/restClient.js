import axios from 'axios';
export class RestClient {
    user;
    pass;
    endpoint;
    constructor(user, pass, endpoint) {
        this.user = user;
        this.pass = pass;
        this.endpoint = endpoint;
    }
    validateID = async (id, type, reason, auditRecord) => {
        const request = {
            auditRecord: auditRecord,
            validateIDInputRecord: {
                id: id,
                type: type,
                reason: reason,
            },
        };
        const options = {
            auth: {
                username: this.user,
                password: this.pass,
            },
        };
        // post the request to the endpoint
        const ed = this.endpoint + '/validateID';
        const response = await axios.post(ed, request, options);
        return response.data;
    };
}
export default RestClient;
