# idValidatorRestServiceClient

This is a client for the idValidatorRestService. It is a REST service that validates a given ID number against a given ID type. Under the scope, it is made calls to other existing KED services for the same purposes transparently.

#### Example
```javascript
import ValidateID from '@digigov-oss/id-validator-rest-service-client';
import config from './config.json';
const test = async () => {
  const validateID = new ValidateID(config.user, config.pass);
  const result = await validateID.validateID('ΑΔ100000', 1,'test');
  console.log(result);
};
```

* you can use `overrides` to override the default values for auditrecord and input fields.
* for your tests, you don't need to use the `overrides` mechanism, in that case, the default storage path will be used ie `/tmp`
>Note: On windows you might face the error `"/tmp/sequence" Path not found.` You can set `overrides.auditStoragePath` property to a folder that exists on your system.

* look at [KED](https://www.gsis.gr/dimosia-dioikisi/ked/) standard guides for records you can use on auditInit"
Also, you can use `overrides` to override the default storage engine. Look at module [AuditRecordDB](https://gitlab.grnet.gr/digigov-oss/auditRecordDB/-/blob/main/README.md) for more details on how to use the AuditEngine.


#### Returns
```
{ kedResponse: { validateIDOutputRecord: { isFoundFlag: true, isValidFlag: true, idOut: 'ΑΔ100000', issueDate: '01/01/2012', name: 'ΔΟΚ-ΟΝΟΜΑ', surname: 'ΔΟΚ-ΕΠΩΝΥΜΟ', fatherName: 'ΔΟΚ-ΠΑΤΡΩΝΥΜΟ', motherName: 'ΔΟΚ - ΜΗΤΡΩΝΥΜΟ', nameLatin: 'DOK-ONOMA', surnameLatin: 'DOK-EPONYMO', fatherNameLatin: 'DOK-PATRONYMO', gender: 'Α', birthDate: '22/07/1981', birthPlace: 'ΑΘΗΝΑ', serviceCalled: 'searchIdentitiesById', idTypeDescr: '1 - ΑΤ ΑΣΤΥΝΟΜΙΚΗ ΤΑΥΤΟΤΗΤΑ' }, callSequenceId: 71972008, callSequenceDate: '2023-01-24T12:42:51.480+02:00', errorRecord: {} }, auditRecord: { auditUnit: 'gov.gr', auditTransactionId: '88', auditProtocol: '88/2023-01-24', auditTransactionDate: '2023-01-24T10:42:51Z', auditUserIp: '127.0.0.1', auditUserId: 'system' } }
```

or an error message.
```
{ validateIDOutputRecord: { isFoundFlag: false, serviceCalled: 'searchIdentitiesById', idTypeDescr: '1 - ΑΤ ΑΣΤΥΝΟΜΙΚΗ ΤΑΥΤΟΤΗΤΑ' }, callSequenceId: 71971997, callSequenceDate: '2023-01-24T12:22:09.938+02:00', errorRecord: { errorCode: 'WS_EXTERNAL_ERROR', errorDescr: 'Η μορφή του αριθμού εγγράφου δεν είναι σωστή (μη έγκυρη είσοδος)' } }
```

### Lookup
for your convenience, you can use the following lookup `DOCTYPESLOOKUP` exported from the module.

### Notes
In case of KED advertises wrong endpoint on production you have to use (override) the endpoint: `https://ked.gsis.gr/esb/idValidatorRestService`
You can do that by setting the `endpoint` property on the `overrides` object.
```
const overrides = {
    endpoint: 'https://ked.gsis.gr/esb/idValidatorRestService',
}
```