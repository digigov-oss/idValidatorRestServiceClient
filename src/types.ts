import { AuditRecord } from '@digigov-oss/gsis-audit-record-db';

type Enumerate<
    N extends number,
    Acc extends number[] = [],
> = Acc['length'] extends N
    ? Acc[number]
    : Enumerate<N, [...Acc, Acc['length']]>;
export type IntRange<F extends number, T extends number> = Exclude<
    Enumerate<T>,
    Enumerate<F>
>;

export const DOCTYPESLOOKUP = {
    1: {
        title: 'ΑΤ ΑΣΤΥΝΟΜΙΚΗ ΤΑΥΤΟΤΗΤΑ',
        description: 'ΔΕΛΤΙΟ ΑΣΤΥΝΟΜΙΚΗΣ ΤΑΥΤΟΤΗΤΑΣ ΠΟΛΙΤΩΝ',
        authority: 'ΕΛΛΗΝΙΚΗ ΑΣΤΥΝΟΜΙΑ',
        service: 'searchIdentitiesById',
    },
    2: {
        title: 'ΕΣ ΕΛΛΗΝΙΚΟΣ ΣΤΡΑΤΟΣ',
        description: 'ΔΕΛΤΙΟ ΤΑΥΤΟΤΗΤΑΣ ΣΤΕΛΕΧΩΝ ΕΛΛΗΝΙΚΟΥ ΣΤΡΑΤΟΥ',
        authority: 'ΣΤΡΑΤΙΩΤΙΚΗ ΥΠΗΡΕΣΙΑ',
        service: 'searchIdentitiesByIdArmy',
    },
    3: {
        title: 'ΠΝ ΠΟΛΕΜΙΚΟ ΝΑΥΤΙΚΟ',
        description: 'ΔΕΛΤΙΟ ΤΑΥΤΟΤΗΤΑΣ ΣΤΕΛΕΧΩΝ ΠΟΛΕΜΙΚΟΥ ΝΑΥΤΙΚΟΥ',
        authority: 'ΣΤΡΑΤΙΩΤΙΚΗ ΥΠΗΡΕΣΙΑ',
        service: 'searchIdentitiesByIdArmy',
    },
    4: {
        title: 'ΠΑ ΠΟΛΕΜΙΚΗ ΑΕΡΟΠΟΡΙΑ',
        description: 'ΔΕΛΤΙΟ ΤΑΥΤΟΤΗΤΑΣ ΣΤΕΛΕΧΩΝ ΠΟΛΕΜΙΚΗΣ ΑΕΡΟΠΟΡΙΑΣ',
        authority: 'ΣΤΡΑΤΙΩΤΙΚΗ ΥΠΗΡΕΣΙΑ',
        service: 'searchIdentitiesByIdArmy',
    },
    5: {
        title: 'ΕΑ ΕΛΛΗΝΙΚΗ ΑΣΤΥΝΟΜΙΑ',
        description: 'ΔΕΛΤΙΟ ΤΑΥΤΟΤΗΤΑΣ ΕΝΣΤΟΛΩΝ/ΑΣΤΥΝΟΜΙΚΩΝ',
        authority: 'ΕΛΛΗΝΙΚΗ ΑΣΤΥΝΟΜΙΑ',
        service: 'searchIdentitiesByIdPol',
    },
    6: {
        title: 'ΛΣ ΛΙΜΕΝΙΚΟ ΣΩΜΑ',
        description: 'ΔΕΛΤΙΟ ΤΑΥΤΟΤΗΤΑΣ ΣΤΕΛΕΧΩΝ ΛΙΜΕΝΙΚΟΥ ΣΩΜΑΤΟΣ',
        authority: 'ΛΙΜΕΝΙΚΟ ΣΩΜΑ',
        service: 'searchIdentitiesByIdHcg',
    },
    7: {
        title: 'ΠΣ ΠΥΡΟΣΒΕΣΤΙΚΟ ΣΩΜΑ',
        description: 'ΔΕΛΤΙΟ ΤΑΥΤΟΤΗΤΑΣ ΣΤΕΛΕΧΩΝ ΠΥΡΟΣΒΕΣΤΙΚΟΥ ΣΩΜΑΤΟΣ',
        authority: 'ΠΥΡΟΣΒΕΣΤΙΚΗ ΥΠΗΡΕΣΙΑ',
        service: 'searchIdentitiesByIdFire',
    },
    8: {
        title: 'ΔΙ ΔΙΑΒΑΤΗΡΙΟ',
        description: 'ΕΛΛΗΝΙΚΟ ΔΙΑΒΑΤΗΡΙΟ',
        authority: 'ΕΛΛΗΝΙΚΗ ΑΣΤΥΝΟΜΙΑ',
        service: 'findPassportByParameters',
    },
    13: {
        title: 'ΒΕΒΑΙΩΣΗ ΕΥΡΩΠΑΙΩΝ ΠΟΛΙΤΩΝ',
        description: 'EE ΒΕΒΑΙΩΣΗ ΕΓΓΡΑΦΗΣ ΠΟΛΙΤΗ ΚΡΑΤΟΥΣ ΜΕΛΟΥΣ ΕΥΡΩΠΑΙΚΗΣ ΕΝΩΣΗΣ',
        authority: 'ΕΛΛΗΝΙΚΗ ΑΣΤΥΝΟΜΙΑ',
        service: 'getPersonalDetailsEE',
    },
    200: {
        title: 'ΕΣ ΓΕΕΘΑ',
        description: 'ΔΕΛΤΙΟ ΤΑΥΤΟΤΗΤΑΣ ΣΤΕΛΕΧΩΝ ΓΕΕΘΑ',
        authority: 'ΣΤΡΑΤΙΩΤΙΚΗ ΥΠΗΡΕΣΙΑ',
        service: 'searchIdentitiesByIdArmy',
    },
};

export type ErrorRecord = {
    errorCode: string;
    errorDescr: string;
};

export type ValidateIDInputRecord = {
    id: string;
    type: IntRange<1, 9> | 13 | 200;
    reason: string;
};

export type ValidateIDInputRequest = {
    auditRecord: AuditRecord;
    validateIDInputRecord: ValidateIDInputRecord;
};

export type ValidateIDOutputRecord = {
    isFoundFlag: boolean;
    isValidFlag?: boolean;
    idOut?: string;
    issueDate?: string;
    issueInstitutionId?: string;
    issueInstitutionDesc?: string;
    expDate?: string;
    name?: string;
    surname?: string;
    fatherName?: string;
    motherName?: string;
    nameLatin?: string;
    surnameLatin?: string;
    fatherNameLatin?: string;
    gender?: string;
    birthDate?: string;
    birthPlace?: string;
    serviceCalled: string;
    idTypeDescr: string;
    message?: string;
};

export type ValidateIDResponse = {
    validateIDOutputRecord: ValidateIDOutputRecord;
    callSequenceId: string;
    callSequenceDate: string;
    errorRecord?: ErrorRecord;
};
export type KEDResponse = ValidateIDResponse;
export type ValidateIDOutput = {
    kedResponse: KEDResponse;
    auditRecord: AuditRecord;
};
