import type { IntRange, ValidateIDOutput, KEDResponse } from './types.js';
import {
    generateAuditRecord,
    AuditRecord,
    FileEngine,
    AuditEngine,
} from '@digigov-oss/gsis-audit-record-db';
import { RestClient } from './restClient.js';
import config from './config.json';

/**
 * @type Overrides
 * @description Overrides for the REST client
 * @param {string} endpoint - Endpoint to be used for the REST client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 * @param {AuditEngine} auditEngine - Audit engine to be used for the audit record produced
 */
export type Overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};

class ValidateID {
    endpoint: string;
    prod: boolean;
    auditInit: object;
    auditStoragePath: string;
    auditEngine: AuditEngine;
    user: string;
    pass: string;

    constructor(user: string, pass: string, overrides?: Overrides) {
        this.prod = overrides?.prod ?? false;
        if (overrides?.endpoint) { 
            this.endpoint = overrides?.endpoint; 
        } else { 
            this.endpoint =  this.prod ? config.prod : config.test;
        }
        this.auditInit = overrides?.auditInit ?? ({} as AuditRecord);
        this.auditStoragePath = overrides?.auditStoragePath ?? '/tmp';
        this.auditEngine =
            overrides?.auditEngine ?? new FileEngine(this.auditStoragePath);
        this.user = user;
        this.pass = pass;
    }

    genAuditRecord = async (overrides?: Overrides): Promise<AuditRecord> => {
        const auditInit = Object.assign(
            {},
            this.auditInit,
            overrides?.auditInit,
        );
        const auditRecord = await generateAuditRecord(
            auditInit,
            this.auditEngine,
        );
        if (!auditRecord) throw new Error('Audit record is not initialized');
        return auditRecord;
    };

    validateID = async (
        id: string,
        type: IntRange<1, 9> | 200,
        reason: string,
        overrides?: Overrides,
    ): Promise<ValidateIDOutput> => {
        const auditRecord = await this.genAuditRecord(overrides);
        const restClient = new RestClient(this.user, this.pass, this.endpoint);
        const output: KEDResponse = await restClient.validateID(
            id,
            type,
            reason,
            auditRecord,
        );
        return { kedResponse: output, auditRecord: auditRecord };
    };
}

export { DOCTYPESLOOKUP } from './types.js';

export type {
    ValidateIDInputRecord,
    ValidateIDResponse,
    ValidateIDOutputRecord,
    ValidateIDOutput,
    IntRange,
    KEDResponse,
    ErrorRecord,
} from './types.js';

export default ValidateID;
