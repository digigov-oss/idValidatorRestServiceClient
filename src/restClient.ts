import type { AuditRecord } from '@digigov-oss/gsis-audit-record-db';
import type {
    IntRange,
    ValidateIDResponse,
    ValidateIDInputRequest,
} from './types';
import axios from 'axios';

export class RestClient {
    user: string;
    pass: string;
    endpoint: string;

    constructor(user: string, pass: string, endpoint: string) {
        this.user = user;
        this.pass = pass;
        this.endpoint = endpoint;
    }

    public validateID = async (
        id: string,
        type: IntRange<1, 9> | 13 | 200,
        reason: string,
        auditRecord: AuditRecord,
    ): Promise<ValidateIDResponse> => {
        const request: ValidateIDInputRequest = {
            auditRecord: auditRecord,
            validateIDInputRecord: {
                id: id,
                type: type,
                reason: reason,
            },
        };
        const options = {
            auth: {
                username: this.user,
                password: this.pass,
            },
        };
        // post the request to the endpoint
        const ed = this.endpoint + '/validateID';
        const response = await axios.post(ed, request, options);
        return response.data as ValidateIDResponse;
    };
}

export default RestClient;
