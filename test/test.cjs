const ValidateID = require('../dist/cjs/index.js').default;
const config = require ('./config.json');
const inspect = require('object-inspect');

const inputs = [
    {
        id: 'ΑΔ100000',
        type: 1,
        reason: 'test' 
    },
    {
        id: '99999',
        type: 7,
        reason: 'test' 
    },
    {
        id: 'ΑΔ100001',
        type: 1,
        reason: 'test' 
    },
    {
        id: 'AB1234567',
        type: 8,
        reason: 'test' 
    },
    {
        id: '1254',
        type: 5,
        reason: 'test' 
    },
    {
        id: 'ΑΔ12345',
        type: 5,
        reason: 'test' 
    },
    {
        id: "0",
        type: 13,
        reason: 'test' 
    },
    {
        id: "2",
        type: 13,
        reason: 'test' 
    },
    {
        id: "GR0777358",
        type: 13,
        reason: 'test' 
    },
    {
        id: "GR0777234",
        type: 13,
        reason: 'test' 
    },
    {
        id: "GR0777000",
        type: 13,
        reason: 'test' 
    }
]

const test = async () => {
    const validateID = new ValidateID(config.user, config.pass);
    for (const input of inputs){
    const response = await validateID.validateID(input.id, input.type, input.reason);
    console.log(inspect(response,
        { depth: 10 },));
    }
}
test();